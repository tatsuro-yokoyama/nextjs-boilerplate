export default function Head() {
  return (
    <>
      <title>My Boilerplate</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="description" content="Generated by My Boilerplate" />
      <link rel="icon" href="/favicon.ico" />
    </>
  )
}
